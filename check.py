# Mailman sensitive settings monitor
# Copyright © 2019 Free Software Foundation Europe
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import configparser
from datetime import datetime
from glob import glob
import logging
from os import stat
import yaml
import requests
from utils import (
    check_password,
    get_mailman_passwords,
    parse_expected_mailing_list_config,
    send_email,
)

config = configparser.ConfigParser()
config.read("config.cfg")
gitea_token = config["GITEA"]["token"]
smtp_default_receiver_email = config["SMTP"]["default_receiver_email"]
actual_config_path = config["MAILMAN_CONFIG"]["path"]
ml_ref_file_path = config["MAILMAN_CONFIG"]["ref_file_path"]

logging.basicConfig(
    level=logging.INFO, format="%(asctime)-15s %(levelname)-6s %(message)s"
)
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description="Check mailman sensitive settings")

# Don't send an email if the --dry-run flag is used
parser.add_argument(
    "--dry-run",
    dest="dry_run",
    action="store_true",
    help="if present, don't send emails if something is misconfigured",
)

args = parser.parse_args()

with open(actual_config_path, "r") as fh:
    actual_config = yaml.load(fh, Loader=yaml.SafeLoader)

response = requests.get(
    "https://git.fsfe.org/api/v1/repos/FSFE/internal/raw/master/"
    + ml_ref_file_path
    + "?access_token="
    + gitea_token
)

if response.status_code != 200:
    logger.fatal("The gitea API returned {0} code".format(str(response.status_code)))
    exit(1)

expected_config, _ = parse_expected_mailing_list_config(response.text)

# Remove the mailing lists where we don't have a match in expected_config
unknown_lists = [ml for ml in actual_config if ml not in expected_config]
if len(unknown_lists) > 0:
    logger.warning(
        "The following lists are not stored in the reference file: "
        + str(unknown_lists)
    )
    actual_config = dict(
        filter(lambda ml: ml[0] not in unknown_lists, actual_config.items())
    )

for mailing_list, actual_options in sorted(
    actual_config.items(), key=lambda config: config[0]
):
    logger.info("Checking config for mailing list: {0}".format(mailing_list))
    # Get the email of the mailing list owners, failback to
    # smtp_default_receiver_email
    email = actual_options["Admins"].replace(" ", "").split(",")
    recipient = email if email[0] != "" else [smtp_default_receiver_email]

    # Make sure the hashed password is the expected one. Also check if a
    # moderator password is set
    (
        hashed_mailing_list_admin_password,
        hashed_mailing_list_mod_password,
    ) = get_mailman_passwords(mailing_list)
    if "notavailable" not in expected_config[mailing_list][
        "Password"
    ] and not check_password(
        expected_config[mailing_list]["Password"].encode(),
        hashed_mailing_list_admin_password,
    ):
        subject = "{0} has the wrong password configured".format(mailing_list)
        message = "Please check that the password is correct"
        logger.error(subject)
        logger.error(message)
        if not args.dry_run:
            send_email(subject, message, recipient)

    if hashed_mailing_list_mod_password != "None":
        logger.warning("{0} has a moderator password set".format(mailing_list))

    # Check if there are old messages in the moderation q
    today = datetime.now()
    msgs_in_q = glob(
        "/var/lib/mailman/data/heldmsg-" + mailing_list + "-" + "[0-9]" + "*.pck"
    )
    msgs_relative_date = [
        (today - datetime.fromtimestamp(stat(file).st_ctime)).days for file in msgs_in_q
    ]
    old_messages = list(
        filter(lambda msg_relative_date: msg_relative_date > 14, msgs_relative_date)
    )
    really_old_messages = list(
        filter(lambda msg_relative_date: msg_relative_date > 30, old_messages)
    )
    if len(really_old_messages) > 0:
        message = "{0} has {1} messages waiting in the moderation queue for more than a month".format(
            mailing_list, len(really_old_messages)
        )
        logger.error(message)
        if not args.dry_run:
            send_email(message, message, recipient + [smtp_default_receiver_email])
    if len(old_messages) > 0:
        message = "{0} has {1} messages waiting in the moderation queue for more than 2 weeks".format(
            mailing_list, len(old_messages)
        )
        logger.warning(message)
        if not args.dry_run:
            send_email(message, message, recipient)

    if "Archives" in expected_config[mailing_list]:
        if (
            "private" in expected_config[mailing_list]["Archives"]
            and not actual_options["archive_private"] == "1"
        ):
            subject = "{0} should have private archives".format(mailing_list)
            message = "Actual config:\n{0}\n====================\nExpected config:\n{1}".format(
                actual_options, expected_config[mailing_list]
            )
            logger.error(subject)
            logger.error(message)
            if not args.dry_run:
                send_email(subject, message, recipient)
        elif (
            "public" in expected_config[mailing_list]["Archives"]
            and not actual_options["archive_private"] == "0"
        ):
            subject = "{0} should have public archives".format(mailing_list)
            message = "Actual config:\n{0}\n====================\nExpected config:\n{1}".format(
                actual_options, expected_config[mailing_list]
            )
            logger.error(subject)
            logger.error(message)
            if not args.dry_run:
                send_email(subject, message, recipient)
        else:
            logger.info("Archives are well configured")

    if "Subscription" in expected_config[mailing_list]:
        if (
            "open" in expected_config[mailing_list]["Subscription"]
            and not actual_options["subscribe_policy"] == "1"
        ):
            subject = "{0} should have subscription open for everyone".format(
                mailing_list
            )
            message = "Actual config:\n{0}\n====================\nExpected config:\n{1}".format(
                actual_options, expected_config[mailing_list]
            )
            logger.error(subject)
            logger.error(message)
            if not args.dry_run:
                send_email(subject, message, recipient)
        elif (
            "private" in expected_config[mailing_list]["Subscription"]
            or "moderated" in expected_config[mailing_list]["Subscription"]
            or "close" in expected_config[mailing_list]["Subscription"]
        ) and actual_options["subscribe_policy"] == "1":
            subject = "{0} should require subscription approval".format(mailing_list)
            message = "Actual config:\n{0}\n====================\nExpected config:\n{1}".format(
                actual_options, expected_config[mailing_list]
            )
            logger.error(subject)
            logger.error(message)
            if not args.dry_run:
                send_email(subject, message, recipient)
        else:
            logger.info("Subscriptions are well configured")
