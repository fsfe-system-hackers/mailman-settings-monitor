<!--
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2019 Free Software Foundation Europe
-->

# Mailman settings monitoring

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/mailman-settings-monitor/00_README)
[![Build Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/mailman-settings-monitor/status.svg)](https://drone.fsfe.org/fsfe-system-hackers/mailman-settings-monitor)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/mailman-settings-monitor)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/mailman-settings-monitor)

## Goal

The goal is to ensure that the FSFE's mailing list sensitive settings are properly
configured.

## How it works?

It works in three steps:

* [export.py](export.py) export the sensitive part of the mailing lists
  configuration to a machine and human readable format
* [update\_ref\_file.py.py](update_ref_file.py) uses the mailman configuration to
  update our mailing list reference file (expected configuration)
* [check.py](check.py) compare the actual configuration to the expected
  configuration, stored in the [internal](https://git.fsfe.org/FSFE/internal)
  repository. Each difference triggers an email to the mailing list
  administrators and a CC email address. It also verifies the password

The sensitive attributes of a mailing list are:

* `private_roster`
* `subscribe_policy`

Details about those attributes can be found [here](https://wiki.list.org/DOC/Mailman%202.1%20List%20Administrators%20Manual#Subscription_rules).

and:

* `archive`
* `archive_privat`

Details about those attributes can be found [here](https://wiki.list.org/DOC/Mailman%202.1%20List%20Administrators%20Manual#Archiving_Options).

## How it use it?

### Requirements

The scripts use python 3 and two libraries:

* yaml (to store the mailing list configuration)
* requests (to get the expected mailing list configuration from git)

You can install them on Debian with the packages `python3-yaml` and
`python3-requests`.

The [export.py](export.py) scripts needs a custom mailman script in your `PATH`.
Download it from
[here](https://git.fsfe.org/fsfe-system-hackers/mailman-tools/src/branch/master/list_modsadmins).


### Configuration

The [check.py](check.py) script uses a configuration file. Move
`config.cfg.dist` to `config.cfg` and change the values according to your
setup.

### Run

Once the requirements are satisfied and the configuration file is ready, run the
[export.py](export.py) script and redirect its output to a file (this must be
done directly on the mailman server):


```bash
python3 export.py > actual_config
```
Then run the [update\_ref\_file.py](update_ref_file.py.py) script:

```bash
python3 update_ref_file.py > output
```
`output` can be considered as the current state of the mailman configuration.

Then run the [check.py](check.py) script:

```bash
python3 check.py
```

## Deployment with Ansible

Once you have adjusted the settings in `config.cfg`, you can use the [Ansible
playbook](playbook.yml) to deploy the script to a remote mailman server
