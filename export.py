# Mailman sensitive settings monitor
# Copyright © 2019 Free Software Foundation Europe
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import yaml
from subprocess import check_output
import re

email_regex = re.compile(r"[^@]+@[^@]+\.[^@]+")

options_to_monitor = [
    "description",
    # https://wiki.list.org/DOC/Mailman%202.1%20List%20Administrators%20Manual#Subscription_rules
    "private_roster",
    "subscribe_policy",
    # https://wiki.list.org/DOC/Mailman%202.1%20List%20Administrators%20Manual#Archiving_Options
    "archive",
    "archive_private",
]

config_to_export = {}

for mailing_list in [
    list.rstrip("\n") for list in os.popen("/usr/sbin/list_lists --bare").readlines()
]:
    mailing_list = mailing_list.lower()
    output = check_output(["/usr/sbin/config_list", "-v", "-o", "/dev/stdout", mailing_list])

    # Decode the config as UTF-8 and ignore errors
    config = output.decode("utf-8", errors="ignore").splitlines()
    # Create the dict options_to_monitor:value for the mailing list
    config_to_export[mailing_list] = dict(
        option.rstrip("\n").replace("'", "").replace('"', "").split(" = ")
        for option in config
        if option.split(" = ")[0] in options_to_monitor
    )
    # Custom script to get both the admins and moderators of a mailing list
    # Please see https://git.fsfe.org/fsfe-system-hackers/mailman-tools/src/branch/master/list_modsadmins
    list_mods_admins = check_output(
        ["/var/lib/mailman/bin/list_modsadmins", mailing_list]
    )
    list_mods_admins = (
        list_mods_admins.decode("ISO-8859-1").rstrip("\n").split(";")[1:3]
    )
    config_to_export[mailing_list]["Admins"], config_to_export[mailing_list]["Mods"] = (
        list_mods_admins[0].split(": ")[1].strip(),
        list_mods_admins[1].split(": ")[1].strip(),
    )

# default_flow_style creates the "recursive" yaml
# width prevents yaml.dump() from inserting new linew in the middle of long
# strings
print(
    yaml.dump(config_to_export, default_flow_style=False, allow_unicode=True, width=200)
)
