# Mailman sensitive settings monitor
# Copyright © 2019 Free Software Foundation Europe
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from collections import OrderedDict
import configparser
from hashlib import sha1
import re
import smtplib
from subprocess import check_output
from typing import Optional

# Use a regex to keep the script portable
email_regex = re.compile(r"[^@]+@[^@]+\.[^@]+")

config = configparser.ConfigParser()
config.read("config.cfg")
smtp_port = int(config["SMTP"]["port"])
smtp_server = config["SMTP"]["server"]
smtp_sender_email = config["SMTP"]["sender_email"]
smtp_cc_email = [config["SMTP"]["cc_email"]]


def send_email(subject: str, text: str, recipient: list) -> bool:
    """send an email to inform

    :subject: the email subject
    :text: the email body
    :returns: True
    """

    message = "Subject: {0}\nTo: {1}\nCc: {2}\n\n\n{3}".format(
        subject, ", ".join(recipient), ", ".join(smtp_cc_email), text
    )

    with smtplib.SMTP(smtp_server, smtp_port) as server:
        server.starttls()
        server.sendmail(smtp_sender_email, recipient + smtp_cc_email, message)
        server.quit()

    return True


def parse_expected_mailing_list_config(mailing_list_settings: str) -> dict:
    """Parse the mailing lists expected settings

    :mailing_list_settings: The settings
    :returns: A parsed representation of the settings and the category of each ml
    """

    expected_config = OrderedDict()
    current_list = ""
    ml_active = True

    # Keep track of the mailing list categories. Each key of the dict is a
    # category and the values of the dict is a list of mailing lists
    categories = OrderedDict()
    category = ""

    for line in mailing_list_settings.splitlines():
        line = line.rstrip("\n")
        # Categories are the only lines that don't start with a space and start
        # with a capital letter
        if line != "" and line[0].isupper():
            category = line
        # Mailing lists names are the mailing lists email addresses
        if email_regex.match(line.rstrip()) and not line.startswith(" "):
            # It's the key which matches the actual settings and the
            # expected ones
            current_list = line.split("@")[0]
            expected_config[current_list] = dict()
            expected_config[current_list]["active"] = ml_active
            expected_config[current_list]["category"] = category
            categories[category] = (
                categories[category] + [current_list]
                if category in categories
                else [current_list]
            )
        # If the line starts with a space it's a mailing list property
        elif line.startswith(" "):
            # Split the line to get a key-value pair
            line_splitted = line.split(": ", 1)
            try:
                expected_config[current_list][line_splitted[0].strip()] = line_splitted[1]
            except IndexError:
                expected_config[current_list][line_splitted[0].strip()] = ""

        # Mark everything under 'Inactive Lists' as inactive mailing lists
        elif line.startswith("Inactive Lists"):
            ml_active = False

    return expected_config, categories


def pretty_print_config(k: str, v: str, prev_v: Optional[str] = None) -> list:
    if k == "archive":
        if str(v) == "True" or v == "1":
            return ["Store Archives", "True"]
        else:
            return ["Store Archives", "False"]
    if k == "archive_private":
        # If the ml is not archived, don't output a archive access policy
        # "archive_private" comes after "archive"
        if str(prev_v) == "False" or prev_v == "0":
            return ["Archives", "none"]
        elif str(v) == "1":
            return ["Archives", "private"]
        else:
            return ["Archives", "public"]
    if k == "subscribe_policy":
        if int(v) <= 1:
            return ["Subscription", "open"]
        else:
            return ["Subscription", "moderated"]
    if k == "description":
        return ["Desc", v if v else "not available"]

    return [k, v]


def get_mailman_passwords(mailing_list: str):
    """Get the hashed password of the mailing list stored by mailman

    :mailing_list: The list's name
    :returns: The hashed passowrd of the list

    """
    output = check_output(["/var/lib/mailman/bin/print_pw", "-l", mailing_list])
    match = re.search(
        r".*passwords: admin=([0-9a-f]+).*mod=(None|[0-9a-f]+)", str(output).strip()
    )

    return match.group(1), match.group(2)


def check_password(password: bytes, mailman_hash: str) -> bool:
    """Compare the password to the hased version stored by mailman

    :password: The list's password (in raw bytes)
    :mailman_hash: the sha1 hashed password

    :returns: Whether the passwords match
    """

    return mailman_hash == sha1(password).hexdigest()
