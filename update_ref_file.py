# Mailman sensitive settings monitor
# Copyright © 2019 Free Software Foundation Europe
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import configparser
import logging
import yaml
import requests
from utils import parse_expected_mailing_list_config, pretty_print_config

config = configparser.ConfigParser()
config.read("config.cfg")
gitea_token = config["GITEA"]["token"]
smtp_default_receiver_email = config["SMTP"]["default_receiver_email"]
actual_config_path = config["MAILMAN_CONFIG"]["path"]
ml_ref_file_path = config["MAILMAN_CONFIG"]["ref_file_path"]

logging.basicConfig(
    level=logging.INFO, format="%(asctime)-15s %(levelname)-6s %(message)s"
)
logger = logging.getLogger(__name__)

with open(actual_config_path, "r") as fh:
    actual_config = yaml.load(fh, Loader=yaml.SafeLoader)

# We get the existing reference file because some fields cannot be easily
# retrieved from the mailman configuration
response = requests.get(
    "https://git.fsfe.org/api/v1/repos/FSFE/internal/raw/master/"
    + ml_ref_file_path
    + "?access_token="
    + gitea_token
)

if response.status_code != 200:
    logger.fatal("The gitea API returned {0} code".format(str(response.status_code)))
    exit(1)

expected_config, categories = parse_expected_mailing_list_config(response.text)

# The attributes we want to print for a mailing list
options = [
    "description",
    "Admins",
    "Mods",
    "subscribe_policy",
    "archive",
    "archive_private",
]

for category, mailing_lists in categories.items():

    print("=" * len(category) + "\n" + category + "\n" + "=" * len(category) + "\n")

    # For each mailing list in a mailing list category, print helpful
    # informations
    for mailing_list in mailing_lists:

        # If the mailing list is not in the mailman configuration
        if mailing_list not in actual_config:
            expected_config[mailing_list]["active"] = False
            print(
                mailing_list
                + "@lists.fsfe.org"
                + "\n "
                # Print all unknown mailing list properties
                + "\n ".join(
                    [
                        ": ".join([k, str(v).strip()])
                        for k, v in expected_config[mailing_list].items()
                    ]
                )
                + "\n"
            )

        else:
            expected_config_options = expected_config[mailing_list]

            print(
                mailing_list
                + "@lists.fsfe.org"
                + "\n "
                + "\n ".join(
                    [
                        ": ".join(
                            pretty_print_config(
                                k,
                                actual_config[mailing_list][k],
                                actual_config[mailing_list][options[idx]],
                            )
                        )
                        # idx is used to pass the value of the previous option
                        for idx, k in enumerate(options, start=-1)
                    ]
                )
                + "\n "
                + "Password: {0}".format(
                    expected_config_options["Password"]
                    if "Password" in expected_config_options
                    else ""
                )
                + "\n"
            )
